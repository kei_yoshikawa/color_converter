#!/usr/bin/env python
# coding: utf-8
from argparse import ArgumentParser
import glob
import os
import sys

import cv2


def get_option():
    parser = ArgumentParser()
    parser.add_argument('-out', '--output_dir',
                        default=os.getcwd(),
                        help='Output directory')
    parser.add_argument('-in', '--input',
                        default='',
                        help='Input file or dorectory path')
    parser.add_argument('-i', '--intencity', type=int,
                        default=0,
                        help='Color of dark part. 0 - 254')
    parser.add_argument('-inv', '--invert', type=bool,
                        default=False,
                        help='Flag to invert color')
    parser.add_argument('-v', '--preview', type=bool,
                        default=False,
                        help='Flag to show preview')

    return parser.parse_args()


class ImageConverter(object):
    """Class to create an converted image"""

    def __init__(self, input_path, output_dir, intencity, invert, show):
        # check input info
        if input_path == '':
            print("'input' is not set.")

        self.input_path = ''
        if os.path.exists(input_path):
            if os.path.isfile(input_path) or os.path.isdir(input_path):
                self.input_path = input_path
            else:
                print("'input' is invalid: " + input_path)

        if self.input_path == '':
            sys.exit(-1)

        # create output dir
        self.output_dir = output_dir
        if os.path.exists(output_dir):
            print("Output dir already exists: " + output_dir)
        else:
            print("Create output dir: " + output_dir)
            os.makedirs(output_dir)

        # set color
        if intencity < 0:
            print("'intencity' should be 0- 255. Use 0.")
            self.intencity = 0
        elif intencity > 254:
            print("'intencity' should be 0- 254. Use 254.")
            self.intencity = 254
        else:
            self.intencity = intencity

        # set flag
        self.invert_flag = invert
        self.show = show

    def convert_image(self, image, intencity):
        """Convert"""
        try:
            # get binary image
            _, image = cv2.threshold(image, 100, 255, cv2.THRESH_BINARY)

            # invert and set color
            if self.invert_flag:
                _, image = cv2.threshold(
                    image, 100, 255 - intencity, cv2.THRESH_BINARY)
            else:
                _, image = cv2.threshold(
                    image, 100, 255 - intencity, cv2.THRESH_BINARY_INV)

            image = cv2.bitwise_not(image)

            if self.show:
                cv2.imshow('image', image)
                cv2.waitKey(0)

        except Exception as e:
            print(e)
            return None

        return image

    def process_image(self, input_path, output_dir, intencity, invert_flag):
        """Main process"""
        try:
            image = cv2.imread(input_path)
        except Exception as e:
            print(e)
            return

        output_image = self.convert_image(image, intencity)

        if output_image is None:
            print("Failed to convert image.")
            return

        output_path = os.path.join(output_dir, os.path.basename(input_path))
        cv2.imwrite(output_path, output_image)

        return

    def run(self):
        if os.path.isfile(self.input_path):
            self.process_image(
                self.input_path, self.output_dir,
                self.intencity, self.invert_flag)

        else:
            image_paths = glob.glob(os.path.join(self.input_path, '*.png')) \
                          + glob.glob(os.path.join(self.input_path, '*.jpg')) \
                          + glob.glob(os.path.join(self.input_path, '*.jpeg'))
            print(str(len(image_paths)) + " image files in input dir.")

            image_paths.sort()
            for image_path in image_paths:
                self.process_image(
                    image_path, self.output_dir,
                    self.intencity, self.invert_flag)

        print("Process done.")

        return


if __name__ == '__main__':
    args = get_option()

    image_converter = ImageConverter(
        args.input, args.output_dir, args.intencity, args.invert, args.preview)
    image_converter.run()
