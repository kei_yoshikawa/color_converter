### Overview

通常の白黒マーカを編集したマーカを作成して保存する。

行える編集は①白と黒を反転および②黒の濃さの変更。
真っ白（255）とグレー（0〜254で指定）で構成されたマーカが保存される。


### Parameter
  -out OUTPUT_DIR, --output_dir
       出力先ディレクトリ。ないときは作成される。
       デフォルトは現在のディレクトリ。
  -in INPUT, --input
      対象となるマーカの画像ファイルのパス。
      あるいはそれらが置かれているディレクトリのパス。ディレクトリが指定されたとき、その中の画像ファイルはすべて編集対象になる。
       指定必須。
  -i INTENCITY, --intencity
     グレー部分の濃さ。0 - 254で指定すること。
     0なら真っ黒、254ならほぼ真っ白。
     デフォルトは0。
  -inv INVERT, --invert
       白黒を反転するかどうかのフラグ。
       デフォルトはFalse（指定しなかったときは反転しない）。


### Usage

引数を与えて呼び出す。引数の順番は問わない。

例：

「/home/toyota/data/marker/10cm」に置かれているマーカを(-in)、
色の濃い部分が輝度100(-i)の反転マーカとして(-inv)
「/home/toyota/data/marker/output」に保存したい(-out)とき

```
python3 convert.py -out /home/toyota/data/marker/output -in /home/toyota/data/marker/10cm -i 100 -inv True
または
python3 convert.py --output /home/toyota/data/marker/output --input /home/toyota/data/marker/10cm --intencity 100 --invert True
など
```

「/home/toyota/data/marker/10cm」に置かれているマーカを(-in)、
真っ黒・輝度0(-i)の通常マーカとして(-inv)
「/home/toyota/data/marker/output」に保存したい(-out)とき

```
python3 convert.py -out /home/toyota/data/marker/output -in /home/toyota/data/marker/10cm -i 0 -inv False
または
python3 convert.py --output /home/toyota/data/marker/output --input /home/toyota/data/marker/10cm
など
```
